--function clr(color)
--  color = color or 'github_dark_high_contrast'
--  vim.cmd.colorscheme(color)
--  vim.api.nvim_set_hl(0, 'Normal', {bg = 'none'})
--end
--clr()
function clr(color)
  color = color or 'github_dark_high_contrast'
  vim.cmd.colorscheme(color)
  vim.api.nvim_set_hl(0, 'Normal', {bg='none'})
  vim.api.nvim_set_hl(0, 'CursorLine', {bg='#505962'})
  vim.api.nvim_set_hl(0, 'Visual', {link='CursorLine'})
  vim.api.nvim_set_hl(0, 'Comment', {fg='#777777'})
  vim.api.nvim_set_hl(0, 'CursorLineNr', {fg='#71b7ff', bold=true})
  vim.api.nvim_set_hl(0, 'InsertMode', {fg='#26cd4d', bold=true})
  vim.api.nvim_set_hl(0, 'VisualMode', {fg='#f0b72f',bold = true})
  vim.api.nvim_set_hl(0, 'LineNr', {fg='#1b2632'})

  vim.api.nvim_set_hl(0, 'NormalNC', {bg='none'})
  vim.api.nvim_set_hl(0, 'NormalFLoat', {bg='none'})
end
clr()
-- return{
--  Type { fg = hsl(20,100,50), gui = "bold" },
--  htmlTag { fg = hsl(30,60,60), gui = 'bold' },
-- }
