local lsp=require('lsp-zero').preset('recommended')
lsp.ensure_installed({'tsserver', 'eslint', 'quick_lint_js', 'tailwindcss', 'rust_analyzer'})

vim.api.nvim_set_hl(0, 'Test', {bg = '#000000', fg = '#000000'})
vim.api.nvim_set_hl(0, 'Norm', {bg = '#000000', fg = '#ffffff'})
vim.api.nvim_set_hl(0, 'Slc', {bg = '#ffffff', bold = true, italic = true})

vim.api.nvim_set_hl(0, 'CmpItemAbbr', {fg = '#8080ff', bg = 'NONE'})
vim.api.nvim_set_hl(0, 'CmpItemAbbrMatch', {fg = '#ccffcc', bg = 'NONE'})
vim.api.nvim_set_hl(0, 'CmpItemAbbrMatchFuzzy', {fg = '#ff3300', bg = 'NONE'})

local cmp = require('cmp')
cmp.setup({
  snippet={
	expand = function(args) vim.fn["vsnip#anonymous"](args.body) end,
  },
  window={
	completion=cmp.config.window.bordered({
	winhighlight='Normal:Test,FloatBorder:Norm,CursorLine:Slc',}),
	completiondocumentation=cmp.config.window.bordered(),
  },
})
local cmp_select = {behavior = cmp.SelectBehavior.Select}
local cmp_mappings = lsp.defaults.cmp_mappings({
  ['<C-k>'] = cmp.mapping.select_prev_item(cmp_select),
  ['<C-j>'] = cmp.mapping.select_next_item(cmp_select),
  ['<C-l>'] = cmp.mapping.confirm({ select = true }),
  ['<C-Space>'] = cmp.mapping.complete(),
})

cmp_mappings['<Tab>'] = nil
cmp_mappings['<S-Tab>'] = nil

lsp.setup_nvim_cmp({
  mapping = cmp_mappings
})

lsp.on_attach(function(client, bufnr)
  local opts = {buffer = bufnr, remap = false}

  vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
  vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end, opts)
  vim.keymap.set("n", "<leader>vca", function() vim.lsp.buf.code_action() end, opts)
  vim.keymap.set("n", "<leader>vrf", function() vim.lsp.buf.references() end, opts)
  vim.keymap.set("n", "<leader>vrn", function() vim.lsp.buf.rename() end, opts)
end)

lsp.setup()
