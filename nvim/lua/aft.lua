local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>t', vim.cmd.Telescope)
vim.keymap.set('n', '<leader>f', builtin.find_files, {})
vim.keymap.set('n', '<leader>/', builtin.live_grep, {})
vim.keymap.set('n', '/', builtin.current_buffer_fuzzy_find, {})
vim.keymap.set('n', '<leader>1', builtin.buffers, {})
vim.keymap.set('n', '<leader>d', builtin.git_status, {})
vim.keymap.set({'n', 't'}, '<leader><leader>', vim.cmd.ToggleTerm)

require("nvim-tree").setup({
  sort_by = "case_sensitive",
  hijack_cursor=true,
  respect_buf_cwd=true,
  update_focused_file={
    enable=true,
  },
  modified={
    enable=true,
    show_on_dirs=true,
  },
  view = {
    side="right",
    width = 10,
  },
  renderer = {
    group_empty = true,
    highlight_git=true,
    highlight_opened_files="all",
    indent_width = 2,
  },
})
require('lualine').setup{
  options = {
    icons_enabled = true,
    globalstatus = true,
    refresh = {
      statusline = 1000,
      tabline = 1000,
      winbar = 1000,
    }
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch','diff'},
    lualine_c = {'diagnostics','searchcount'},
    lualine_x = {''},
    lualine_y = {'fileformat'},
    lualine_z = {'progress'}
  },
}
require('barbecue').setup({
  theme={
    normal = { fg = '#ffffff' },
    context_function = { bold = true },
  }
})
require('gitsigns').setup {
  signs = {
    add          = { text = '│' },
    change       = { text = '│'},
    delete       = { text = '_' },
    topdelete    = { text = '‾' },
    changedelete = { text = '~' },
    untracked    = { text = '┆' },
  },
  signcolumn = true,  -- Toggle with `:Gitsigns toggle_signs`
  numhl      = true, -- Toggle with `:Gitsigns toggle_numhl`
  linehl     = false, -- Toggle with `:Gitsigns toggle_linehl`
  word_diff  = false, -- Toggle with `:Gitsigns toggle_word_diff`
  watch_gitdir = {
    follow_files = true
  },
  attach_to_untracked = true,
  current_line_blame = false, -- Toggle with `:Gitsigns toggle_current_line_blame`
  current_line_blame_opts = {
    virt_text = true,
    virt_text_pos = 'eol', -- 'eol' | 'overlay' | 'right_align'
    delay = 1000,
    ignore_whitespace = false,
  },
  current_line_blame_formatter = '<author>, <author_time:%Y-%m-%d> - <summary>',
  sign_priority = 6,
  update_debounce = 100,
  status_formatter = nil, -- Use default
  preview_config = {
    -- Options passed to nvim_open_win
    border = 'single',
    style = 'minimal',
    relative = 'cursor',
    row = 0,
    col = 1
  },
  yadm = {
    enable = false
  },
}
require("nvim-treesitter.configs").setup{ensure_installed = {"c", "javascript", "html", "typescript", "jsdoc","rust", "python", "cpp", "scss" },sync_install = false, auto_install = true,highlight = { enable=true,additional_vim_regex_highlighting = false}, indent = { enable = true }}
