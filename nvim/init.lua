require('int')
local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
vim.opt.rtp:prepend(lazypath)
vim.o.cursorline = true
vim.o.number = true
require('lazy').setup({{'projekt0n/github-nvim-theme',priority = 100, config = function() require('clr') end},
{'nvim-treesitter/nvim-treesitter', priority=98,build = ':TSUpdate',},
{'nvim-telescope/telescope.nvim',tag='0.1.2', dependencies = { 'nvim-lua/plenary.nvim'}},
{"nvim-tree/nvim-tree.lua",version = "*",lazy = true,dependencies = {"nvim-tree/nvim-web-devicons",}},
 {'nvim-lualine/lualine.nvim', priority=96},
{"utilyre/barbecue.nvim",name="barbecue",version="*",dependencies={'SmiteshP/nvim-navic'}},
 {'VonHeikemen/lsp-zero.nvim',branch = 'v2.x', priority = 97, lazy = true,dependencies = {
  {'neovim/nvim-lspconfig','williamboman/mason-lspconfig.nvim', {'williamboman/mason.nvim', build=function() pcall(vim.cmd, 'MasonUpdate') end}}, 'hrsh7th/nvim-cmp','L3MON4D3/LuaSnip', 'saadparwaiz1/cmp_luasnip', 'hrsh7th/cmp-nvim-lsp', 'hrsh7th/cmp-buffer', 'hrsh7th/cmp-path', 'hrsh7th/cmp-cmdline'}},
  {'Exafunction/codeium.vim',config=function() vim.keymap.set('i', '<C-]>', function() return vim.fn['codeium#CycleCompletions'](-1) end, { expr = true }) end},
"folke/neodev.nvim", "folke/neoconf.nvim",'mbbill/undotree',
{'windwp/nvim-autopairs',event = "InsertEnter",opts = {},},
  'windwp/nvim-ts-autotag',
  {'mawkler/modicator.nvim', config = function() require('modicator').setup()end,},
  {'akinsho/toggleterm.nvim', version = "*", opts={}},
  'lewis6991/gitsigns.nvim'})
require('lsp')
require('aft')
require('nvim-ts-autotag').setup()
