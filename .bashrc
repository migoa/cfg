# If not running interactively, don't do anything
[[ $- != *i* ]] && return
alias cl='clear'
alias l='ls -sh --color'
alias gi='flatpak run io.github.shiftkey.Desktop'
alias c='cd'
# alias grep='grep --color=auto'
alias vim='nvim'

PS1='\[\033[1;31m\]m\033[1;34m\]\w/\[\033[00m\]\033[1;37m\] '

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
